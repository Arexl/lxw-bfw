![](https://github.com/lxw-bfw/lxw-bfw/blob/main/header_.png?raw=true)

![Typing SVG](https://readme-typing-svg.demolab.com?font=Fira+Code&size=16&pause=1000&random=false&width=444&lines=Welcome%2C+visitor.+Wishing+you+a+wonderful+day%E2%9C%A8)

### *Faith*:  Code builds my world. ✨✨✨✨✨✨✨✨✨✨✨

##### 目前状态

>
> 大环境下之前的公司也裁员了，离职后，找新的工作也找了段时间了，期间也研究过一些程序员相关的副业和创业，不过没什么收获，还交了一笔学费，酸Q~😂。目前的话，专业和工作方向主要还是web前端和小程序开发。

##### 个人介绍

> 专注web前端开发，对各类小程序、跨端App开发也有一定了解和开发经验。深知时间就是金钱，效率就是生命，所以对于前端工程化等可以持续提高研发效率和质量的事情也感兴趣。在系列项目开发中，也基于`nodejs`做过一些小的自动化工具，集成到项目工具链中，来帮助项目的一些特殊又繁琐的需求自动化实现。
>
> 今年把更多时间放在了`nodejs`技术栈的web 后台开发，对全栈开发也有了一些更进一步的涉猎，到目前为止做过几个小的全栈项目，项目前端核心技术栈主要是`react`+`typescript`，后台主要是搭建`RESTful`风格的`web api`服务，为多端前端项目提供统一的接口服务，核心技术栈主要是`egg.js`+系列插件(包括基础业务需求、性能、安全和缓存等) + `mongoose` 。数据库以`MongoDB`为主。

**Languages:**  

<code><img height="20" src="./static/javascript.png"></code>
<code><img height="20" src="./static/typescript.png"></code>
<code><img height="20" src="./static/react.png"></code>
<code><img height="20" src="./static/nodejs.png"></code>
<code><img height="20" src="./static/Vue.png"></code>
<code><img height="20" src="./static/flutter.png"></code>
<code><img height="20" src="./static/miniProgram.png"></code>
<code><img height="20" src="./static/less.png"></code>
<code><img height="20" src="./static/css-sass.png"></code>

